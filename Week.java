import java.util.Scanner;

public class Week {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[][] schedule = new String[7][2];

        schedule[0][0] = "sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "tuesday";
        schedule[2][1] = "attend meeting";
        schedule[3][0] = "wednesday";
        schedule[3][1] = "study for exam";
        schedule[4][0] = "thursday";
        schedule[4][1] = "work on project";
        schedule[5][0] = "friday";
        schedule[5][1] = "go to gym";
        schedule[6][0] = "saturday";
        schedule[6][1] = "family outing";


        boolean check = true;
        while (check) {
            System.out.println("Please, input the day of the week or ('exit' to quit): ");
            String input = sc.nextLine().toLowerCase();
            if (input.equals("exit")) {
                break;

            }
            boolean found = false;
            for (int i = 0; i < 7; i++) {
                if (schedule[i][0].equals(input)) {
                    System.out.println("Your tasks for " + schedule[i][0] + ": " + schedule[i][1]);
                    found = true;
                    break;
                }

                if (input.equals("change " + schedule[i][0])) {
                    System.out.println("Please, input new tasks for " + schedule[i][0]);
                    String newTask = sc.nextLine();
                    schedule[i][1] = schedule[i][1]+", "+ newTask;
                    System.out.println("Tasks updated successfully for " + schedule[i][0]);
                    found = true;
                    break;

                }

            }
            if (!found) {
                System.out.println("Sorry, I don't understand you, please try again.");
            }


        }
    }
}
